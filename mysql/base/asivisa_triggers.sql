use asivisa;

DELIMITER $$
CREATE TRIGGER ingestaCaloricaDieta1
    after INSERT ON comidadieta
    FOR EACH ROW BEGIN
		update Dieta
		set ingestaCalorica= (	select sum(Comida.ingestaCalorica) 
								from Comida, ComidaDieta
								where Dieta.nombre=ComidaDieta.dieta and ComidaDieta.comida=Comida.nombre)
		where Dieta.nombre in (	select cd.dieta from ComidaDieta cd
								where Dieta.nombre=cd.dieta); 
	
END$$

DELIMITER $$
CREATE TRIGGER ingestaCaloricaDieta2
    after delete ON comidadieta
    FOR EACH ROW BEGIN

		update Dieta
		set ingestaCalorica= (	select sum(comida.ingestaCalorica) 
								from comida, comidadieta
								where Dieta.nombre=comidadieta.dieta and comidadieta.comida=comida.nombre)
		where Dieta.nombre in (	select cd.dieta from comidadieta cd
								where dieta.nombre=cd.dieta); 
	
END$$

DELIMITER $$
CREATE TRIGGER ingestaCaloricaDieta3
    after update ON comidadieta
    FOR EACH ROW BEGIN

		update dieta
		set ingestaCalorica= (	select sum(comida.ingestaCalorica) 
								from comida, comidadieta
								where dieta.nombre=comidadieta.dieta and comidadieta.comida=comida.nombre)
		where dieta.nombre in (	select cd.dieta from comidadieta cd
								where dieta.nombre=cd.dieta); 
	
END$$


DELIMITER $$
CREATE TRIGGER caloriasQuemadasRutina1
    after insert ON ejerciciorutina
    FOR EACH ROW BEGIN
		update Rutina
		set caloriasQuemadas= (	select sum(ejercicio.caloriasQuemadas) 
								from ejercicio, ejerciciorutina
								where rutina.nombre=ejerciciorutina.rutina and ejerciciorutina.ejercicio=ejercicio.nombre)
		where rutina.nombre in (	select er.rutina from ejerciciorutina er
									where rutina.nombre=er.rutina); 
	
END$$

DELIMITER $$
CREATE TRIGGER caloriasQuemadasRutina2
    after delete ON ejerciciorutina
    FOR EACH ROW BEGIN
		update Rutina
		set caloriasQuemadas= (	select sum(ejercicio.caloriasQuemadas) 
								from ejercicio, ejerciciorutina
								where rutina.nombre=ejerciciorutina.rutina and ejerciciorutina.ejercicio=ejercicio.nombre)
		where rutina.nombre in (	select er.rutina from ejerciciorutina er
									where rutina.nombre=er.rutina); 
	
END$$

DELIMITER $$
CREATE TRIGGER caloriasQuemadasRutina3
    after update ON ejerciciorutina
    FOR EACH ROW BEGIN
		update Rutina
		set caloriasQuemadas= (	select sum(ejercicio.caloriasQuemadas) 
								from ejercicio, ejerciciorutina
								where rutina.nombre=ejerciciorutina.rutina and ejerciciorutina.ejercicio=ejercicio.nombre)
		where rutina.nombre in (	select er.rutina from ejerciciorutina er
									where rutina.nombre=er.rutina); 
	
END$$


DELIMITER $$
CREATE TRIGGER abonado
    after insert ON pago
    FOR EACH ROW BEGIN
		update usuariomembresia
		set abonado= (	select sum(pago.monto) 
								from pago
								where pago.usuario=usuariomembresia.usuario)
		where usuariomembresia.usuario in (	select pago.usuario from pago
									where pago.usuario=usuariomembresia.usuario); 
	
END$$