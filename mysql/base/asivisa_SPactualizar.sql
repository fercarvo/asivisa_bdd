use asivisa;

delimiter //
DROP PROCEDURE IF EXISTS actualizarUsuario//
DELIMITER //
CREATE PROCEDURE actualizarUsuario(nombre varchar(100), usuario varchar(30), telefono varchar(10),
									residencia varchar(30), pesoInicial float, estatura float)
BEGIN
	update Usuario u
	set u.nombre= nombre, u.telefono=telefono, u.residencia=residencia, u.pesoInicial=pesoInicial, u.estatura=estatura 
	where u.usuario=usuario;
  
END //


delimiter //
DROP PROCEDURE IF EXISTS actualizarComida//
DELIMITER //
CREATE PROCEDURE actualizarComida(nombre varchar(20), ingestaCalorica float, descripcion varchar(50))
BEGIN
	update Comida c
	set c.ingestaCalorica= ingestaCalorica, c.descripcion=descripcion
	where c.nombre= nombre;
  
END //


delimiter //
DROP PROCEDURE IF EXISTS actualizarDieta//
DELIMITER //
CREATE PROCEDURE actualizarDieta(nombre varchar(20), horaDia varchar(10))
BEGIN
	update Dieta d
	set d.horaDia=horaDia
	where d.nombre= nombre;
  
END //


delimiter //
DROP PROCEDURE IF EXISTS actualizarEjercicio//
DELIMITER //
CREATE PROCEDURE actualizarEjercicio(nombre varchar(20), descripcion varchar(50), intensidad varchar(8),
										caloriasQuemadas float )
BEGIN
	update Ejercicio e
	set e.descripcion=descripcion, e.intensidad=intensidad, e.caloriasQuemadas=caloriasQuemadas
	where e.nombre= nombre;
  
END //


delimiter //
DROP PROCEDURE IF EXISTS actualizarRutina//
DELIMITER //
CREATE PROCEDURE actualizarRutina(nombre varchar(20), descripcion varchar(50))
BEGIN
	update Rutina r
	set r.descripcion=descripcion
	where r.nombre= nombre;
  
END //



