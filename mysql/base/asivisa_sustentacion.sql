use asivisa;

delimiter //
DROP PROCEDURE IF EXISTS rutinasConEjercicios//
DELIMITER // 
CREATE PROCEDURE rutinasConEjercicios()
BEGIN
	select r.nombre as rutina, e.nombre as ejercicio
	from ejercicio e, ejerciciorutina er, rutina r
	where e.nombre=er.ejercicio and r.nombre=er.rutina
	order by r.nombre;
  
END //

DROP PROCEDURE IF EXISTS rutinasConEjercicios_buscar//
DELIMITER // 
CREATE PROCEDURE rutinasConEjercicios_buscar(nombre varchar(20))
BEGIN
	select e.nombre, e. descripcion, e.caloriasQuemadas
	from Rutina r, ejercicio e, ejerciciorutina er
	where e.nombre=er.ejercicio and r.nombre=er.rutina and
			r.nombre like concat('%',nombre,'%')
	order by e.nombre;
  
END //



call rutinasConEjercicios();
call rutinasConEjercicios();