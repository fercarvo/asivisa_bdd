use asivisa;



create view reporteEjercicio_EnUso as
select 	nombre, descripcion, intensidad, caloriasQuemadas
	from 	ejercicio 
	where	ejercicio.nombre in (select 	e.nombre 
									from 	Usuario u, UsuarioRutina ur, Rutina r, 
											EjercicioRutina er, Ejercicio e 
									where 	u.usuario=ur.usuario and ur.rutina=r.nombre and 
											r.nombre=er.rutina and er.ejercicio=e.nombre);



create view reporteEjercicio_EnDesUso as
select 	nombre, descripcion, intensidad, caloriasQuemadas
	from 	ejercicio 
	where	ejercicio.nombre not in (select e.nombre 
									from 	Usuario u, UsuarioRutina ur, Rutina r, 
											EjercicioRutina er, Ejercicio e 
									where 	u.usuario=ur.usuario and ur.rutina=r.nombre and 
											r.nombre=er.rutina and er.ejercicio=e.nombre);  




create view reporteComida_EnUso as
select 	nombre, ingestaCalorica, descripcion
	from 	comida 
	where	comida.nombre in (select c.nombre 
									from 	Usuario u, usuariodieta ud, Dieta d, 
											comidadieta cd, Comida c 
									where 	u.usuario=ud.usuario and ud.dieta=d.nombre and
											d.nombre=cd.dieta and cd.comida=c.nombre);



create view reporteComida_EnDesUso as
select 	nombre, ingestaCalorica, descripcion
	from 	comida 
	where	comida.nombre not in (select c.nombre 
									from 	Usuario u, usuariodieta ud, Dieta d, 
											comidadieta cd, Comida c 
									where 	u.usuario=ud.usuario and ud.dieta=d.nombre and
											d.nombre=cd.dieta and cd.comida=c.nombre);




create view reporteUsuario_Membresia as 
select u.nombre,u.usuario, u.telefono, u.residencia, um.abonado
from usuario u, membresia m, usuariomembresia um
where 	u.usuario=um.usuario and m.nombre=um.membresia
		and um.abonado<m.costo;