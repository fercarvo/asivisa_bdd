use asivisa;

call crearMembresia("normal", 130, "membresia basica para usuarios");
call crearMembresia("premium", 200, "membresia de alta clase");

call crearUsuario("Fernando carvajal", "fercarvo", "4322123", "cdla alborada 6ta", "60", "173");
call crearUsuario("Ruben carvajal", "rubancar", "4322123", "cdla alborada 6ta", "56", "172");
call crearUsuario("Ruben ulloa", "rulloa", "4322123", "cdla alborada 6ta", "56", "172");
call crearUsuario("Carlx Marxs", "Camax", "5678900", "Antigua rusia", "70", "198");
call crearUsuario("Jefferson Perez ", "jeffe", "7134854", "Cuenca, Sector terminal ", "66", "168");
call crearUsuario("Agustin delgado", "tin", "6678900", "ibarra, balle del chota", "86", "192");
call crearUsuario("Carlos Merchan", "carlosm", "1234567", "Sauces 7 Mz 54", "70", "170");
call crearUsuario("Carlos Alvarado", "caralv", "1234567", "Alborada 7 Mz 54", "70", "160");
call crearUsuario("Carlos Tinoco", "cartic", "2342345", "Orquideas 7 Mz 54", "70", "180");
call crearUsuario("Ruben Merchan", "rubmer", "9878654", "Urdeza 7 Mz 54", "59", "150");
call crearUsuario("Fernando Merchan", "fermer", "4567890", "Mapasingue 7 Mz 54", "70", "120");
call crearUsuario("Erick Merchan", "ericmer", "4324567", "San borondon km 14", "70", "190");
call crearUsuario("Carlos Aguirre", "caraguirrr", "4564534", "Daule Clever franco", "60", "167");
call crearUsuario("Carlos Ulloa", "Carull", "1234567", "Sauces 7 Mz 54", "70", "170");
call crearUsuario("Carlos Larreategui", "carlarre", "1234567", "Alborada 7 Mz 54", "70", "160");
call crearUsuario("Carlos Valencia", "carvalen", "2342345", "Orquideas 7 Mz 54", "70", "180");
call crearUsuario("Ruben Correa", "rucorrea", "4322123", "Urdeza 7 Mz 54", "59", "150");
call crearUsuario("Rafael Merchan", "rumerchn", "1234567", "Alborada 7 Mz 54", "70", "120");
call crearUsuario("Vicente Merchan", "vicenmer", "4324567", "San borondon km 14", "70", "190");
call crearUsuario("Carlos Zurita", "carzurit", "4564534", "Daule Clever franco", "60", "167");


call crearUsuarioMembresia("fercarvo","normal");
call crearUsuarioMembresia("rubancar","normal");
call crearUsuarioMembresia("rulloa","premium");
call crearUsuarioMembresia("Camax","normal");
call crearUsuarioMembresia("jeffe","premium");
call crearUsuarioMembresia("tin","normal");
call crearUsuarioMembresia("carzurit","normal");
call crearUsuarioMembresia("vicenmer","premium");
call crearUsuarioMembresia("rumerchn","normal");
call crearUsuarioMembresia("rucorrea","premium");
call crearUsuarioMembresia("carvalen","normal");
call crearUsuarioMembresia("carlarre","normal");
call crearUsuarioMembresia("Carull","premium");
call crearUsuarioMembresia("caraguirrr","normal");
call crearUsuarioMembresia("ericmer","premium");
call crearUsuarioMembresia("fermer","normal");



call crearPago("fercarvo","pago",30);
call crearPago("rubancar","pago",60);
call crearPago("rulloa","pago",100);
call crearPago("Camax","pago",10);
call crearPago("jeffe","pago",3.4);
call crearPago("tin","pago 2",1);


call crearComida("seco de pollo",13,"plato tipico");
call crearComida("seco de gallina",10,"plato tipico saludable");
call crearComida("seco de carne",20,"plato tipico");
call crearComida("guatita",10,"el plato favorito del loco abdala");
call crearComida("caldo de bola",20,"alto en sabor");
call crearComida("llapingacho",10,"plato tipico");
call crearComida("pollo al ajillo",10,"plato tipico de la costa");
call crearComida("ceviche de camaron",30,"alto en colesterol");
call crearComida("ceviche de concha",10.3,"ideal para el cerebro");
call crearComida("camaron apanado",30.9,"muy alto en colesterol");
call crearComida("seco de chivo",30,"alto en proteinas y minerales");
call crearComida("encebollado",15,"´lato tiíco nacional");
call crearComida("seco de conejo",20,"plato tipico");
call crearComida("guatita frita",10,"el plato favorito del loco abdala");
call crearComida("caldo de pata",20,"alto en sabor");
call crearComida("locro",10,"plato tipico");
call crearComida("pollo al curri",10,"plato tipico de la costa");
call crearComida("ceviche de calamar",30,"alto en colesterol");
call crearComida("ceviche de langosta",10.3,"ideal para el cerebro");
call crearComida("camaron slateado",30.9,"muy alto en colesterol");
call crearComida("seco de vaca",30,"alto en proteinas y minerales");
call crearComida("encebiche",15,"´lato tiíco nacional");



call crearEjercicio("Press de banca","ejercicio de pecho","alto",123);
call crearEjercicio("Mancuernas triceps","ejercicio para triceps","medio",60.4);
call crearEjercicio("Caminar 10 min","ejercicio cardiobascular","bajo",30);
call crearEjercicio("trotar 10 minutos","ejercicio cardiobascular","medio",60);
call crearEjercicio("correr 10 minutos","ejercicio cardiovascular","alto",120.7);
call crearEjercicio("natacion","ejercicio cardiovascular","medio",90);
call crearEjercicio("baloncesto","ejercicio de cardiovascular","alto",70);
call crearEjercicio("Press de pierna","ejercicio de pecho","alto",123);
call crearEjercicio("Mancuernas manos","ejercicio para triceps","medio",60.4);
call crearEjercicio("Caminar 20 min","ejercicio cardiobascular","bajo",30);
call crearEjercicio("trotar 20 minutos","ejercicio cardiobascular","medio",60);
call crearEjercicio("correr 20 minutos","ejercicio cardiovascular","alto",120.7);
call crearEjercicio("natacion libre","ejercicio cardiovascular","medio",90);
call crearEjercicio("baloncesto 90 min","ejercicio de cardiovascular","alto",70);
call crearEjercicio("Press de manos","ejercicio de pecho","alto",123);
call crearEjercicio("Mancuernas hombros","ejercicio para hombros","medio",60.4);
call crearEjercicio("Caminar 30 min","ejercicio cardiobascular","bajo",30);
call crearEjercicio("trotar 30 minutos","ejercicio cardiobascular","medio",60);
call crearEjercicio("correr 30 minutos","ejercicio cardiovascular","alto",120.7);
call crearEjercicio("natacion mariposa","ejercicio cardiovascular","medio",90);


call crearDieta("dieta del tomate", "desalluno");
call crearDieta("dieta proteinica", "almuerzo");
call crearDieta("tradicional", "cena");
call crearDieta("tipica", "almuerzo");
call crearDieta("tradicional tipica", "desalluno");
call crearDieta("definicion 10 dias ", "cena");
call crearDieta("definicion 20 dias", "desalluno");
call crearDieta("definicion 30 dias", "cena");
call crearDieta("engorde", "almuerzo");
call crearDieta("dietetica ", "desalluno");
call crearDieta("tercera edad", "cena");
call crearDieta("dolescentes", "almuerzo");
call crearDieta("diabeticos", "cena");
call crearDieta("diabeticos tipo 2", "desalluno");
call crearDieta("para piernas", "almuerzo");
call crearDieta("engordar gluteos", "cena");
call crearDieta("engordar piernas", "almuerzo");
call crearDieta("definicion piernas", "desalluno");
call crearDieta("definicion gluteos", "almuerzo");
call crearDieta("definicion cintura", "desalluno");

call crearRutina("pecho","rutina de pecho");
call crearRutina("hombro","rutina de hombros");
call crearRutina("biceps","rutina de biceps");
call crearRutina("triceps","rutina de triceps");
call crearRutina("espalda","rutina de espalda");
call crearRutina("cardio definicion","para definir musculo");
call crearRutina("cardio resistencia","rutina para aumentar resistencia");
call crearRutina("cardiobascular"," quemar grasa ");
call crearRutina("definir cintura","para definir y quemar grasa de cintura ");
call crearRutina("definir biceps"," marca biceps");
call crearRutina("definir triceps","marca triceps  ");
call crearRutina("definir antebrazo"," marca el atebrazo ");
call crearRutina("cardio olimpico"," quemar grasa nivel olimpico ");
call crearRutina("cardio tercara edad"," quemar grasa nivel anciano ");
call crearRutina("cardio adolescentes"," quemar grasa para jovenes ");
call crearRutina("cardio principiantes"," quemar grasa principiantes ");
call crearRutina("espalda inversa"," definir espala inversa ");
call crearRutina("hombro superior"," definir hombro superior ");
call crearRutina("hombro inferior"," definir hombro inferior ");
call crearRutina("hombro lateral"," definir hmbro lateral ");
call crearRutina("ABS"," abdomen ");

call crearUsuarioRutina("fercarvo","pecho");
call crearEjercicioRutina("Press de banca", "pecho" );








