use asivisa;

DELIMITER //
CREATE PROCEDURE mostrarUsuario()
BEGIN
	select nombre, usuario, telefono, residencia, pesoInicial, estatura
	from usuario
	order by nombre;
  
END //

#-----------------------------------------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE mostrarComida()
BEGIN
	select nombre, ingestaCalorica, descripcion 
	from Comida
	order by nombre, ingestaCalorica;
  
END //

#------------------------------------------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE mostrarEjercicio()
BEGIN
	select nombre, descripcion, intensidad, caloriasQuemadas 
	from Ejercicio
	order by nombre, caloriasQuemadas;
  
END //



#------------------------------------------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE mostrarDieta()
BEGIN
	select nombre, ingestaCalorica, horaDia 
	from Dieta
	order by nombre;
  
END //


#--------------------------------------------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE mostrarRutina()
BEGIN
	select nombre, descripcion, caloriasQuemadas
	from Rutina
	order by nombre, caloriasQuemadas;
  
END //

#--------------------------------------------------------------------------------------------------#

DELIMITER // 
CREATE PROCEDURE buscarUsuario_nombre(nombre varchar(100) )
BEGIN
	select u.nombre, u.telefono, u.residencia, u.pesoInicial, u.estatura
	from Usuario u
	where	u.nombre like concat('%', nombre, '%')
	order by nombre, residencia;
  
END //

DELIMITER // 
CREATE PROCEDURE buscarUsuario_residencia(residencia varchar(30) )
BEGIN
	select u.nombre, u.telefono, u.residencia, u.pesoInicial, u.estatura
	from Usuario u
	where	u.residencia like concat('%', residencia, '%')
	order by nombre, residencia;
  
END //

DELIMITER // 
CREATE PROCEDURE buscarUsuario_estatura(estatura float )
BEGIN
	select u.nombre, u.telefono, u.residencia, u.pesoInicial, u.estatura
	from Usuario u
	where	u.estatura=estatura
	order by nombre, residencia;
  
END //


DELIMITER // 
CREATE PROCEDURE buscarUsuario_pesoInicial(pesoInicial float)
BEGIN
	select u.nombre, u.telefono, u.residencia, u.pesoInicial, u.estatura
	from Usuario u
	where	u.pesoInicial=pesoInicial
	order by nombre, residencia;
  
END //


#--------------------------------------------------------------------------------------------------#
DROP PROCEDURE IF EXISTS buscarEjercicio_nombre//
DELIMITER // 
CREATE PROCEDURE buscarEjercicio_nombre( nombre varchar(20))
BEGIN
	select e.nombre, e.descripcion, e.intensidad, e.caloriasQuemadas
	from Ejercicio e
	where e.nombre like concat('%', nombre, '%');
  
END //

DROP PROCEDURE IF EXISTS buscarEjercicio_caloriasQuemadas//
DELIMITER // 
CREATE PROCEDURE buscarEjercicio_caloriasQuemadas( caloriasQuemadas float)
BEGIN
	select e.nombre, e.descripcion, e.intensidad, e.caloriasQuemadas
	from Ejercicio e
	where e.caloriasQuemadas=caloriasQuemadas;
  
END //

DROP PROCEDURE IF EXISTS buscarEjercicio_descripcion//
DELIMITER // 
CREATE PROCEDURE buscarEjercicio_descripcion( descripcion varchar(50))
BEGIN
	select e.nombre, e.descripcion, e.intensidad, e.caloriasQuemadas
	from Ejercicio e
	where e.descripcion like concat('%', descripcion, '%');
  
END //

DROP PROCEDURE IF EXISTS buscarEjercicio_intensidad//
DELIMITER // 
CREATE PROCEDURE buscarEjercicio_intensidad( intensidad varchar(8))
BEGIN
	select e.nombre, e.descripcion, e.intensidad, e.caloriasQuemadas
	from Ejercicio e
	where e.intensidad like concat('%', intensidad, '%');
  
END //


#--------------------------------------------------------------------------------------------------#
DROP PROCEDURE IF EXISTS buscarComida_nombre//
DELIMITER // 
CREATE PROCEDURE buscarComida_nombre(nombre varchar(20))
BEGIN
	select c.nombre, c.ingestaCalorica, c.descripcion
	from Comida c
	where c.nombre like concat('%',nombre,'%');
  
END //

DROP PROCEDURE IF EXISTS buscarComida_calorias//
DELIMITER // 
CREATE PROCEDURE buscarComida_calorias(calorias float)
BEGIN
	select c.nombre, c.ingestaCalorica, c.descripcion
	from Comida c
	where c.ingestaCalorica = calorias;
  
END //

DROP PROCEDURE IF EXISTS buscarComida_descripcion//
DELIMITER // 
CREATE PROCEDURE buscarComida_descripcion(descripcion varchar(50))
BEGIN
	select c.nombre, c.ingestaCalorica, c.descripcion
	from Comida c
	where c.descripcion like concat('%',descripcion,'%');
  
END //

#--------------------------------------------------------------------------------------------------#

DROP PROCEDURE IF EXISTS buscarDieta_nombre//
DELIMITER // 
CREATE PROCEDURE buscarDieta_nombre(nombre varchar(20))
BEGIN
	select d.nombre, d.ingestaCalorica, d.horaDia
	from Dieta d
	where d.nombre like concat('%',nombre,'%')
	order by nombre, ingestaCalorica;
  
END //

DROP PROCEDURE IF EXISTS buscarDieta_ingestaCalorica//
DELIMITER // 
CREATE PROCEDURE buscarDieta_ingestaCalorica(ingestaCalorica float)
BEGIN
	select d.nombre, d.ingestaCalorica, d.horaDia
	from Dieta d
	where d.ingestaCalorica=ingestaCalorica
	order by nombre, ingestaCalorica;
  
END //

DROP PROCEDURE IF EXISTS buscarDieta_horaDia//
DELIMITER // 
CREATE PROCEDURE buscarDieta_horaDia(horaDia varchar(10))
BEGIN
	select d.nombre, d.ingestaCalorica, d.horaDia
	from Dieta d
	where d.horaDia like concat('%',horaDia,'%')
	order by nombre, ingestaCalorica;
  
END //

#-------------------------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS buscarRutina_nombre//
DELIMITER // 
CREATE PROCEDURE buscarRutina_nombre(nombre varchar(20))
BEGIN
	select r.nombre, r.descripcion, r.caloriasQuemadas
	from Rutina r
	where r.nombre like concat('%',nombre,'%')
	order by nombre, caloriasQuemadas;
  
END //

DROP PROCEDURE IF EXISTS buscarRutina_descripcion//
DELIMITER // 
CREATE PROCEDURE buscarRutina_descripcion(descripcion varchar(50))
BEGIN
	select r.nombre, r.descripcion, r.caloriasQuemadas
	from Rutina r
	where r.descripcion like concat('%',descripcion,'%')
	order by nombre, caloriasQuemadas;
  
END //

DROP PROCEDURE IF EXISTS buscarRutina_caloriasQuemadas//
DELIMITER // 
CREATE PROCEDURE buscarRutina_caloriasQuemadas(caloriasQuemadas float)
BEGIN
	select r.nombre, r.descripcion, r.caloriasQuemadas
	from Rutina r
	where r.caloriasQuemadas=caloriasQuemadas
	order by nombre, caloriasQuemadas;
  
END //

#-------------------------------------------------------------------------------------------------#

DELIMITER //
DROP PROCEDURE IF EXISTS reporteEjercicio_EnUso//
DELIMITER //
CREATE PROCEDURE reporteEjercicio_EnUso()
BEGIN
		select * from reporteEjercicio_EnUso;
END //

#-------------------------------------------------------------------------------------------------#

DELIMITER //
DROP PROCEDURE IF EXISTS reporteEjercicio_EnDesUso//
DELIMITER //
CREATE PROCEDURE reporteEjercicio_EnDesUso()
BEGIN
		select * from reporteEjercicio_EnDesUso;
END //

#-------------------------------------------------------------------------------------------------#

DELIMITER //
DROP PROCEDURE IF EXISTS reporteComida_EnUso//
DELIMITER //
CREATE PROCEDURE reporteComida_EnUso()
BEGIN
		select * from reporteComida_EnUso;
END //

#-------------------------------------------------------------------------------------------------#

DELIMITER //
DROP PROCEDURE IF EXISTS reporteComida_EnDesUso//
DELIMITER //
CREATE PROCEDURE reporteComida_EnDesUso()
BEGIN
		select * from reporteComida_EnDesUso;
END //

#-------------------------------------------------------------------------------------------------#

DELIMITER //
DROP PROCEDURE IF EXISTS reporteUsuario_Membresia//
DELIMITER //
CREATE PROCEDURE reporteUsuario_Membresia()
BEGIN
		select * from reporteUsuario_Membresia;
END //

