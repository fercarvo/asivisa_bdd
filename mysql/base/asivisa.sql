
drop database asivisa;


CREATE database asivisa;
USE asivisa;

CREATE TABLE Ejercicio(
	idEjercicio INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR (20) NOT NULL,
	descripcion VARCHAR (50) NOT NULL,
	intensidad VARCHAR (8) NOT NULL,
	caloriasQuemadas FLOAT NOT NULL,
	CONSTRAINT CHECK(intensidad in ('bajo','medio','alto')),
	UNIQUE (nombre),
	PRIMARY KEY (idEjercicio)
);

CREATE TABLE Rutina(
	idRutina INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR (20) NOT NULL, 
	descripcion VARCHAR (50) NOT NULL,
	caloriasQuemadas float,
	PRIMARY KEY (idRutina),
	UNIQUE (nombre)
);

CREATE TABLE EjercicioRutina(
	ejercicio varchar(20) NOT NULL , 
	rutina varchar(20) NOT NULL,
	FOREIGN KEY (ejercicio) 
		REFERENCES Ejercicio(nombre)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (rutina)
		REFERENCES Rutina(nombre)
		ON UPDATE CASCADE ON DELETE CASCADE,
	unique(ejercicio, rutina)
);


CREATE TABLE Usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR (100) NOT NULL,
	usuario VARCHAR(30) NOT NULL,
	telefono VARCHAR(10) NOT NULL,
	residencia VARCHAR (30) NOT NULL,
	pesoInicial float,
	estatura float,
	unique(usuario),
	PRIMARY KEY (idUsuario)
);

CREATE TABLE Dieta(
	idDieta INT AUTO_INCREMENT NOT NULL,
	nombre VARCHAR (20) NOT NULL,
	ingestaCalorica FLOAT,
	horaDia VARCHAR (10) NOT NULL,
	CONSTRAINT CHECK (horaDia in ('desalluno','almuerzo','merienda','cena')),
	UNIQUE (nombre),
	PRIMARY KEY (idDieta)
);

CREATE TABLE Comida(
	idComida INT AUTO_INCREMENT NOT NULL,
	nombre VARCHAR (20) NOT NULL,
    ingestaCalorica FLOAT,
	descripcion VARCHAR (50) NOT NULL,
	UNIQUE (nombre),
	PRIMARY KEY (idComida)
);

CREATE TABLE ComidaDieta(
	comida varchar(20) NOT NULL,
	dieta varchar(20) NOT NULL,
	FOREIGN KEY (comida)
		REFERENCES Comida(nombre)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (dieta)
		REFERENCES Dieta(nombre)
		ON UPDATE CASCADE ON DELETE CASCADE,
	unique(comida, dieta)
);

CREATE TABLE UsuarioRutina(
	rutina varchar(20) NOT NULL,
	usuario varchar(30) NOT NULL,
	FOREIGN KEY (usuario)
		REFERENCES Usuario(usuario)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (rutina)
		REFERENCES Rutina(nombre)
		ON UPDATE CASCADE ON DELETE CASCADE,
	unique(rutina, usuario)
);

CREATE TABLE UsuarioDieta(
	dieta varchar(20) NOT NULL,
	usuario varchar(30) NOT NULL,
	FOREIGN KEY (dieta)
		REFERENCES Dieta(nombre)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (usuario)
		REFERENCES Usuario(usuario)
		ON UPDATE CASCADE ON DELETE CASCADE,
	unique(dieta, usuario)
);

CREATE TABLE Membresia(
	nombre varchar(50),
	costo float not null,
	descripcion varchar(100),
	primary key(nombre)
);


CREATE TABLE UsuarioMembresia(
	id INT AUTO_INCREMENT,
	usuario varchar(30) not null,
	membresia VARCHAR (50) not null,
	abonado float,
	PRIMARY KEY (id),
	unique(usuario),
	FOREIGN KEY (usuario)
		REFERENCES Usuario(usuario)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (membresia)
		REFERENCES Membresia(nombre)
		ON UPDATE CASCADE ON DELETE CASCADE
	
);


CREATE TABLE Pago (
    fecha DATE,
	usuario varchar(30) not null,
    descripcion VARCHAR(100),
    monto float,
	FOREIGN KEY (usuario)
		REFERENCES UsuarioMembresia(usuario)
		ON UPDATE CASCADE ON DELETE CASCADE
);



