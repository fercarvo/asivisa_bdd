use asivisa;

#---------------------------------------------------------------------------------------#

drop procedure if exists eliminarUsuario;
DELIMITER //
CREATE PROCEDURE eliminarUsuario(in usuario varchar(30))
BEGIN
	delete from Usuario
	where Usuario.usuario=usuario;
  
END //

DROP PROCEDURE IF EXISTS actualizarUsuario//
DELIMITER //
CREATE PROCEDURE actualizarUsuario(	in nombre varchar(100), in usuario varchar(30), 
									in telefono varchar(10), in residencia varchar(30), 
									in pesoInicial float, in estatura float)
BEGIN
	update Usuario 
	set telefono=telefono, residencia=residencia, pesoInicial=pesoInicial, estatura=estatura
	where Usuario.usuario=usuario and Usuario.nombre=nombre;
  
END //

#---------------------------------------------------------------------------------------#
DROP PROCEDURE IF EXISTS eliminarComida//
DELIMITER //
CREATE PROCEDURE eliminarComida( in nombre varchar(20))
BEGIN
	delete from Comida
	where Comida.nombre=nombre;
  
END //

DROP PROCEDURE IF EXISTS actualizarComida//
DELIMITER //
CREATE PROCEDURE actualizarComida(in nombre varchar(20), in ingestaCalorica float, in descripcion varchar(50))
BEGIN
	update Comida 
	set ingestaCalorica=ingestaCalorica, descripcion=descripcion
	where Comida.nombre=nombre;
  
END //

#---------------------------------------------------------------------------------------#

DROP PROCEDURE IF EXISTS eliminarEjercicio//
DELIMITER //
CREATE PROCEDURE eliminarEjercicio(in nombre varchar(20))
BEGIN
	delete from Ejercicio
	where Ejercicio.nombre=nombre;
  
END //

DROP PROCEDURE IF EXISTS actualizarEjercicio//
DELIMITER //
CREATE PROCEDURE actualizarEjercicio(	in nombre varchar(20), in descripcion varchar(50), 
										in intensidad varchar(8), in caloriasQuemadas float)
BEGIN
	update Ejercicio
	set descripcion=descripcion, intensidad=intensidad, caloriasQuemadas=caloriasQuemadas
	where Ejercicio.nombre=nombre;
  
END //

#---------------------------------------------------------------------------------------#

DROP PROCEDURE IF EXISTS eliminarDieta//
DELIMITER //
CREATE PROCEDURE eliminarDieta(in nombre varchar(20))
BEGIN
	delete from Dieta
	where Dieta.nombre=nombre;
  
END //

DROP PROCEDURE IF EXISTS actualizarDieta//
DELIMITER //
CREATE PROCEDURE actualizarDieta(	in nombre varchar(20), in horaDia varchar(10))
BEGIN
	update Dieta
	set nombre=nombre, horaDia=horaDia 
	where Dieta.nombre=nombre;
  
END //

#---------------------------------------------------------------------------------------#

DROP PROCEDURE IF EXISTS eliminarRutina//
DELIMITER //
CREATE PROCEDURE eliminarRutina(in nombre varchar(20))
BEGIN
	delete from Rutina
	where Rutina.nombre=nombre;
  
END //

DROP PROCEDURE IF EXISTS actualizarRutina//
DELIMITER //
CREATE PROCEDURE actualizarRutina(in nombre varchar(20), in descripcion varchar (50), fechaInicio date)
BEGIN
	update Rutina
	set nombre=nombre, descripcion=descripcion, fechaInicio=fechaInicio
	where Rutina.nombre=nombre;
  
END //