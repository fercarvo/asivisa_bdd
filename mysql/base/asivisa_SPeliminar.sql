use asivisa;

delimiter //
DROP PROCEDURE IF EXISTS eliminarComida//
DELIMITER // 
CREATE PROCEDURE eliminarComida( nombre varchar(20))
BEGIN
	delete from Comida
	where Comida.nombre=nombre;
  
END //

delimiter //
DROP PROCEDURE IF EXISTS eliminarDieta//
DELIMITER // 
CREATE PROCEDURE eliminarDieta( nombre varchar(20))
BEGIN
	delete from Dieta
	where Dieta.nombre=nombre;
  
END //

delimiter //
DROP PROCEDURE IF EXISTS eliminarUsuario//
DELIMITER // 
CREATE PROCEDURE eliminarUsuario( usuario varchar(30))
BEGIN
	delete from Usuario
	where Usuario.usuario=usuario;
  
END //

delimiter //
DROP PROCEDURE IF EXISTS eliminarRutina//
DELIMITER // 
CREATE PROCEDURE eliminarRutina( nombre varchar(20))
BEGIN
	delete from Rutina
	where Rutina.nombre=nombre;
  
END //

delimiter //
DROP PROCEDURE IF EXISTS eliminarEjercicio//
DELIMITER // 
CREATE PROCEDURE eliminarEjercicio( nombre varchar(20))
BEGIN
	delete from Ejercicio
	where Ejercicio.nombre=nombre;
  
END //

delimiter //
DROP PROCEDURE IF EXISTS eliminarEjercicioRutina//
DELIMITER //
CREATE PROCEDURE eliminarEjercicioRutina(ejercicio varchar(20), rutina varchar(20))
BEGIN
	delete from EjercicioRutina
	where EjercicioRutina.ejercicio= ejercicio and EjercicioRutina.rutina=rutina;
  
END //


delimiter //
DROP PROCEDURE IF EXISTS eliminarComidaDieta//
DELIMITER //
CREATE PROCEDURE eliminarComidaDieta(comida varchar(20), dieta varchar(20))
BEGIN
	delete from comidadieta
	where comidadieta.comida= comida and comidadieta.dieta=dieta;
  
END //


delimiter //
DROP PROCEDURE IF EXISTS eliminarUsuarioRutina//
DELIMITER //
CREATE PROCEDURE eliminarUsuarioRutina(usuario varchar(30), rutina varchar(20))
BEGIN
	delete from UsuarioRutina
	where UsuarioRutina.usuario= usuario and UsuarioRutina.rutina=rutina;
  
END //


delimiter //
DROP PROCEDURE IF EXISTS EliminarUsuarioDIeta//
DELIMITER //
CREATE PROCEDURE EliminarUsuarioDIeta(usuario varchar(30), dieta varchar(20))
BEGIN
	delete from UsuarioDieta
	where UsuarioDieta.dieta= dieta and UsuarioDieta.usuario=usuario;
  
END //