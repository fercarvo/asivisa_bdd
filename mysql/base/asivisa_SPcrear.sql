use asivisa;

DELIMITER //
CREATE PROCEDURE crearUsuario(	in nombre varchar(39), in usuario varchar(30), in telefono varchar(10),
								in residencia varchar(30), 
								in pesoInicial float, in estatura float)
BEGIN
	Insert into Usuario (nombre, usuario, telefono, residencia, pesoInicial, estatura)
	VALUES(nombre, usuario, telefono, residencia, pesoInicial, estatura);
  
END //

#---------------------------------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE crearComida(in nombre varchar(20), in ingestaCalorica float, in descripcion varchar(50))
BEGIN
	Insert into Comida (nombre, ingestacalorica, descripcion)
	VALUES(nombre, ingestacalorica, descripcion);
  
END //

#---------------------------------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE crearEjercicio(in nombre varchar(20), in descripcion varchar(100), in intensidad varchar(8),
								in caloriasQuemadas float)
BEGIN
	Insert into Ejercicio(nombre, descripcion, intensidad, caloriasQuemadas)
	VALUES(nombre, descripcion, intensidad, caloriasQuemadas);
  
END //

#---------------------------------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE crearDieta(in nombre varchar(30), in horaDia varchar(10))
BEGIN
	Insert into DIeta (nombre, horaDia)
	VALUES(nombre, horaDia);
  
END //

#---------------------------------------------------------------------------------------#

DELIMITER //
CREATE PROCEDURE crearRutina(in nombre varchar(30), in descripcion varchar(50))
BEGIN
	Insert into Rutina (nombre, descripcion)
	VALUES(nombre, descripcion);
  
END //

#---------------------------------------------------------------------------------------#

delimiter //
DROP PROCEDURE IF EXISTS crearEjercicioRutina//
DELIMITER //
CREATE PROCEDURE crearEjercicioRutina(ejercicio varchar(20), rutina varchar(20))
BEGIN
	Insert into EjercicioRutina (ejercicio, rutina)
	VALUES(ejercicio, rutina);
  
END //


#---------------------------------------------------------------------------------------#

delimiter //
DROP PROCEDURE IF EXISTS crearComidaDieta//
DELIMITER //
CREATE PROCEDURE crearComidaDieta(comida varchar(20), dieta varchar(20))
BEGIN
	Insert into ComidaDieta (comida, dieta)
	VALUES(comida, dieta);
  
END //

#---------------------------------------------------------------------------------------#

delimiter //
DROP PROCEDURE IF EXISTS crearUsuarioDieta//
DELIMITER //
CREATE PROCEDURE crearUsuarioDieta(usuario varchar(30), dieta varchar(20))
BEGIN
	Insert into usuariodieta (usuario, dieta)
	VALUES(usuario, dieta);
  
END //

#---------------------------------------------------------------------------------------#

delimiter //
DROP PROCEDURE IF EXISTS crearUsuarioRutina//
DELIMITER //
CREATE PROCEDURE crearUsuarioRutina(usuario varchar(30), rutina varchar(20))
BEGIN
	Insert into UsuarioRutina (usuario, rutina)
	VALUES(usuario, rutina);
  
END //

#---------------------------------------------------------------------------------------#

delimiter //
DROP PROCEDURE IF EXISTS crearMembresia//
DELIMITER //
CREATE PROCEDURE crearMembresia(nombre varchar(50), costo float, descripcion varchar(100))
BEGIN
	Insert into membresia VALUES(nombre, costo, descripcion);
  
END //


#---------------------------------------------------------------------------------------#

delimiter //
DROP PROCEDURE IF EXISTS crearUsuarioMembresia//
DELIMITER //
CREATE PROCEDURE crearUsuarioMembresia(usuario varchar(30), membresia varchar(50))
BEGIN
	Insert into usuariomembresia(usuario, membresia) VALUES(usuario, membresia);
  
END //


#---------------------------------------------------------------------------------------#

delimiter //
DROP PROCEDURE IF EXISTS crearPago//
DELIMITER //
CREATE PROCEDURE crearPago(usuario varchar(30), descripcion varchar(100), monto float)
BEGIN
	Insert into Pago(fecha, usuario, descripcion, monto) 
	VALUES( curdate(),usuario, descripcion, monto);
  
END //
