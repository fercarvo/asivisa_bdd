/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */

package mysql.manejador;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */

public class Conector {

    private final String bd = "asivisa";
    private final String login = "root";
    private final String password = "1991";
    private final String url = "jdbc:mysql://localhost:3306/"+bd;
    private Connection conn = null;
    
    
    public void conectar(){
        try{         
            Class.forName("com.mysql.jdbc.Driver");         
            conn = DriverManager.getConnection(url,login,password);
            System.out.println("Se conecto");
        }catch(SQLException | ClassNotFoundException e){
            System.out.println(e);
        }
    }

    public void desconectar(){
        try {
            this.conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    public void crearComida(String nombre, float ingestaCalorica, String descripcion){
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL crearComida(?,?,?)");
            proc.setString("nombre", nombre);
            proc.setFloat("ingestaCalorica", ingestaCalorica);
            proc.setString("descripcion",descripcion);
            proc.execute();
            this.desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void crearEjercicio(String nombre, String descripcion, String intensidad, float calorias){
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL crearEjercicio(?,?,?,?) ");
            proc.setString("nombre", nombre);
            proc.setString("descripcion", descripcion);
            proc.setString("intensidad", intensidad);
            proc.setFloat("caloriasQuemadas", calorias);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void crearDieta(String nombre, String horaDia){
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL crearDieta(?,?) ");
            proc.setString("nombre", nombre);
            proc.setString("horaDia", horaDia);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void crearRutina(String nombre, String descripcion){
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL crearRutina(?,?) ");
            proc.setString("nombre", nombre);
            proc.setString("descripcion", descripcion);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void crearUsuario(String nombre, String usuario, String telefono, String residencia, float pesoInicial, float estatura){
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL crearDieta(?,?) ");
            proc.setString("nombre", nombre);
            proc.setString("usuario",usuario );
            proc.setString("telefono", telefono );
            proc.setString("residencia", residencia );
            proc.setFloat("pesoInicial", pesoInicial );
            proc.setFloat("estatura", estatura );
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public ResultSet mostrarComida(){
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL mostrarComida()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;    
    }
    
    public ResultSet mostrarDieta(){
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL mostrarDieta()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }
    
    public ResultSet mostrarEjercicio(){
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL mostrarEjercicio");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }
    
    public ResultSet mostrarRutina(){
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL mostrarRutina()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }
    
    public ResultSet mostrarUsuario(){
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL mostrarUsuario()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public void actualizarEjercicio(String nombre, String descripcion, String intensidad, float caloriasQuemadas) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL actualizarEjercicio(?,?,?,?) ");
            proc.setString("nombre", nombre);
            proc.setString("descripcion",descripcion );
            proc.setString("intensidad", intensidad );
            proc.setFloat("caloriasQuemadas", caloriasQuemadas );
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void actualizarRutina(String nombre, String descripcion){
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL actualizarRutina(?,?) ");
            proc.setString("nombre", nombre);
            proc.setString("descripcion",descripcion );
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void actualizarDieta(String nombre, String horaDia){
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL actualizarDieta(?,?) ");
            proc.setString("nombre", nombre);
            proc.setString("horaDia",horaDia );
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }    
    }
    
    public void actualizarComida(String nombre, float ingestaCalorica, String descripcion){
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL actualizarComida(?,?,?) ");
            proc.setString("nombre", nombre);
            proc.setFloat("ingestaCalorica",ingestaCalorica );
            proc.setString("descripcion", descripcion );
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void actualizarUsuario(String nombre, String usuario, String telefono, String residencia, float pesoInicial, float estatura){
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL actualizarEjercicio(?,?,?) ");
            proc.setString("nombre", nombre);
            proc.setString("usuario",usuario );
            proc.setString("telefono",telefono );
            proc.setString("residencia",residencia );
            proc.setFloat("pesoInicial", pesoInicial );
            proc.setFloat("estatura", estatura );
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public ResultSet buscarComida_nombre(String nombre) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarComida_nombre(?)");
            proc.setString("nombre", nombre);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarComida_calorias(float calorias) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarComida_calorias(?)");
            proc.setFloat("calorias", calorias);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarComida_descripcion(String descricpion) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarComida_descripcion(?)");
            proc.setString("descripcion", descricpion);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }    

    public ResultSet buscarEjercicio_nombre(String nombre) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarEjercicio_nombre(?)");
            proc.setString("nombre", nombre);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarEjercicio_caloriasQuemadas(float calorias) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarEjercicio_caloriasQuemadas(?)");
            proc.setFloat("caloriasQuemadas", calorias);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarEjercicio_descripcion(String descripion) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarEjercicio_descripcion(?)");
            proc.setString("descripcion", descripion);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarEjercicio_intensidad(String intensidad) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarEjercicio_intensidad(?)");
            proc.setString("intensidad", intensidad);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarUsuario_nombre(String nombre) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarUsuario_nombre(?)");
            proc.setString("nombre", nombre);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarUsuario_pesoInicial(float pesoINicial) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarUsuario_pesoInicial(?)");
            proc.setFloat("pesoInicial", pesoINicial);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarUsuario_residencia(String residencia) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarUsuario_residencia(?)");
            proc.setString("residencia", residencia);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarUsuario_estatura(float estatura) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarUsuario_estatura(?)");
            proc.setFloat("estatura", estatura);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarDieta_nombre(String nombre) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarDieta_nombre(?)");
            proc.setString("nombre", nombre);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarDieta_ingestaCalorica(float calorias) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarDieta_ingestaCalorica(?)");
            proc.setFloat("ingestaCalorica", calorias);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarDieta_horaDia(String horaDia) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarDieta_horaDia(?)");
            proc.setString("horaDia", horaDia);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarRutina_nombre(String nombre) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarRutina_nombre(?)");
            proc.setString("nombre", nombre);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarRutina_descripcion(String descripcion) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarRutina_descripcion(?)");
            proc.setString("descripcion", descripcion);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet buscarRutina_calorias(float calorias) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL buscarRutina_calorias(?)");
            proc.setFloat("calorias", calorias);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public void eliminarDieta(String nombre) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL eliminarDieta(?) ");
            proc.setString("nombre", nombre);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminarEjercicio(String nombre) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL eliminarEjercicio(?) ");
            proc.setString("nombre", nombre);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminarComida(String nombre) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL eliminarComida(?)");
            proc.setString("nombre", nombre);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminarUsuario(String usuario) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL eliminarUsuario(?) ");
            proc.setString("usuario", usuario);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void eliminarRutina(String nombre) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL eliminarRutina(?) ");
            proc.setString("nombre", nombre);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se actualizo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void crearEjercicioRutina(String ejercicio, String rutina) {
         try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL crearEjercicioRutina(?,?) ");
            proc.setString("ejercicio", ejercicio);
            proc.setString("rutina", rutina);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void eliminarEjercicioRutina(String ejercicio, String rutina) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL eliminarEjercicioRutina(?,?) ");
            proc.setString("ejercicio", ejercicio);
            proc.setString("rutina", rutina);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void crearComidaDieta(String comida, String dieta) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL crearComidaDieta(?,?) ");
            proc.setString("comida", comida);
            proc.setString("dieta", dieta);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void eliminarComidaDieta(String comida, String dieta) {
        try {   
            conectar();
            CallableStatement proc = conn.prepareCall(" CALL eliminarComidaDieta(?,?) ");
            proc.setString("comida", comida);
            proc.setString("dieta", dieta);
            proc.execute();            
            desconectar();
        } 
        catch(SQLException sqlex){
            JOptionPane.showMessageDialog(null, "No se creo: "+sqlex.getMessage(), "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }

    public ResultSet reporteEjercicio_EnUso() {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL reporteEjercicio_EnUso()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet reporteEjercicio_EnDesUso() {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL reporteEjercicio_EnDesUso()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
        
    }

    public ResultSet reporteComida_EnDesUso() {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL reporteComida_EnDesUso()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet reporteComida_EnUso() {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL reporteComida_EnUso()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet reporteUsuario_Membresia() {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL reporteUsuario_Membresia()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }

    public ResultSet rutinasConEjercicios() {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL rutinasConEjercicios()");
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;   
    }

    public ResultSet rutinasConEjercicios_buscar(String rutina) {
        try {   
            this.conectar();
            CallableStatement proc = conn.prepareCall("CALL rutinasConEjercicios_buscar(?)");
            proc.setString("nombre", rutina);
            ResultSet re= proc.executeQuery();
            return re;
        }catch (Exception e) {                  
            
        }
        return null;
    }
}