/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */
package principal;

import javax.swing.UIManager;
import ventanas.VentanaPrincipal;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */

public class Main {

    public static void main(String[] args) {
        
        try { 
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"); 
        } catch (Exception e) { 
        }

        VentanaPrincipal vp = new VentanaPrincipal();
        vp.setVisible(true);
    }
    
}
