/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */

package ventanas;

import vistas.editarDatos.VistaEditarComida;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class VentanaEditarDatos extends javax.swing.JFrame {

    public VentanaEditarDatos(VentanaPrincipal vp) {
        initComponents();
        this.vp=vp;
        vistaEditarComida= new VistaEditarComida();
        jTabbedPaneComida.addTab("Comida", vistaEditarComida);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPaneComida = new javax.swing.JTabbedPane();
        vistaEditarDieta1 = new vistas.editarDatos.VistaEditarDieta();
        vistaEditarEjercicio1 = new vistas.editarDatos.VistaEditarEjercicio();
        vistaEditarRutina1 = new vistas.editarDatos.VistaEditarRutina();
        vistaEditarEjercicioRutina1 = new vistas.editarDatos.VistaEditarEjercicioRutina();
        vistaEditarComidaDieta1 = new vistas.editarDatos.VistaEditarComidaDieta();

        jTabbedPaneComida.addTab("Dieta", vistaEditarDieta1);
        jTabbedPaneComida.addTab("Ejercicio", vistaEditarEjercicio1);
        jTabbedPaneComida.addTab("Rutina", vistaEditarRutina1);
        jTabbedPaneComida.addTab("Ejercicio rutina", vistaEditarEjercicioRutina1);
        jTabbedPaneComida.addTab("Comida Dieta", vistaEditarComidaDieta1);

        getContentPane().add(jTabbedPaneComida, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane jTabbedPaneComida;
    private vistas.editarDatos.VistaEditarComidaDieta vistaEditarComidaDieta1;
    private vistas.editarDatos.VistaEditarDieta vistaEditarDieta1;
    private vistas.editarDatos.VistaEditarEjercicio vistaEditarEjercicio1;
    private vistas.editarDatos.VistaEditarEjercicioRutina vistaEditarEjercicioRutina1;
    private vistas.editarDatos.VistaEditarRutina vistaEditarRutina1;
    // End of variables declaration//GEN-END:variables
    private VistaEditarComida vistaEditarComida;
    private VentanaPrincipal vp;
    
}
