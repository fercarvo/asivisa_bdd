/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */

package ventanas;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class VentanaReportes extends javax.swing.JFrame {


    public VentanaReportes(VentanaPrincipal vp) {
        initComponents();
        this.vp=vp;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        reporteComidaEnDesUso1 = new vistas.reportes.ReporteComidaEnDesUso();
        reporteComidaEnUso1 = new vistas.reportes.ReporteComidaEnUso();
        reporteEjercicioEnDesUso1 = new vistas.reportes.ReporteEjercicioEnDesUso();
        reporteEjercicioEnUso1 = new vistas.reportes.ReporteEjercicioEnUso();
        reporteUsuarioMembresia1 = new vistas.reportes.ReporteUsuarioMembresia();

        jTabbedPane1.addTab("Comida en uso", reporteComidaEnDesUso1);
        jTabbedPane1.addTab("Comida en des-uso", reporteComidaEnUso1);
        jTabbedPane1.addTab("Ejercicios en des-uso", reporteEjercicioEnDesUso1);
        jTabbedPane1.addTab("Ejercicios en uso", reporteEjercicioEnUso1);
        jTabbedPane1.addTab("Usuarios deudores", reporteUsuarioMembresia1);

        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane jTabbedPane1;
    private vistas.reportes.ReporteComidaEnDesUso reporteComidaEnDesUso1;
    private vistas.reportes.ReporteComidaEnUso reporteComidaEnUso1;
    private vistas.reportes.ReporteEjercicioEnDesUso reporteEjercicioEnDesUso1;
    private vistas.reportes.ReporteEjercicioEnUso reporteEjercicioEnUso1;
    private vistas.reportes.ReporteUsuarioMembresia reporteUsuarioMembresia1;
    // End of variables declaration//GEN-END:variables
    private VentanaPrincipal vp;
}
