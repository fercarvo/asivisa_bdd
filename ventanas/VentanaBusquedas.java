/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */

package ventanas;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class VentanaBusquedas extends javax.swing.JFrame {

    public VentanaBusquedas(VentanaPrincipal vp) {
        initComponents();
        this.vp= vp;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JTabbedBusqueda = new javax.swing.JTabbedPane();
        vistaBusquedaComida1 = new vistas.busquedas.VistaBusquedaComida();
        vistaBusquedaEjercicio1 = new vistas.busquedas.VistaBusquedaEjercicio();
        vistaBusquedaUsuario1 = new vistas.busquedas.VistaBusquedaUsuario();
        vistaBusquedaDieta1 = new vistas.busquedas.VistaBusquedaDieta();
        vistaBusquedaRutina1 = new vistas.busquedas.VistaBusquedaRutina();

        setPreferredSize(new java.awt.Dimension(500, 550));

        JTabbedBusqueda.addTab("Comida", vistaBusquedaComida1);
        JTabbedBusqueda.addTab("Ejercicio", vistaBusquedaEjercicio1);
        JTabbedBusqueda.addTab("Usuario", vistaBusquedaUsuario1);
        JTabbedBusqueda.addTab("Dieta", vistaBusquedaDieta1);
        JTabbedBusqueda.addTab("Rutina", vistaBusquedaRutina1);

        getContentPane().add(JTabbedBusqueda, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane JTabbedBusqueda;
    private vistas.busquedas.VistaBusquedaComida vistaBusquedaComida1;
    private vistas.busquedas.VistaBusquedaDieta vistaBusquedaDieta1;
    private vistas.busquedas.VistaBusquedaEjercicio vistaBusquedaEjercicio1;
    private vistas.busquedas.VistaBusquedaRutina vistaBusquedaRutina1;
    private vistas.busquedas.VistaBusquedaUsuario vistaBusquedaUsuario1;
    // End of variables declaration//GEN-END:variables
    private VentanaPrincipal vp;
}
