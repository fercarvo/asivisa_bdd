/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */

package ventanas;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class VentanaMostrarDatos extends javax.swing.JFrame {

    public VentanaMostrarDatos(VentanaPrincipal vp) {
        initComponents();
        this.vp=vp;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        vistaUsuario3 = new vistas.mostrarDatos.VistaUsuario();
        vistaRutina1 = new vistas.mostrarDatos.VistaRutina();
        vistaEjercicio2 = new vistas.mostrarDatos.VistaEjercicio();
        vistaDieta2 = new vistas.mostrarDatos.VistaDieta();
        vistaComida2 = new vistas.mostrarDatos.VistaComida();
        sustentacionRutinaEjercicio1 = new vistas.mostrarDatos.SustentacionRutinaEjercicio();
        sustentacionRutinaEjercicoBuscar1 = new vistas.mostrarDatos.SustentacionRutinaEjercicoBuscar();

        setTitle("Datos del servidor");

        jTabbedPane1.addTab("Usuario", vistaUsuario3);
        jTabbedPane1.addTab("Rutina", vistaRutina1);
        jTabbedPane1.addTab("Ejercicio", vistaEjercicio2);
        jTabbedPane1.addTab("Dieta", vistaDieta2);
        jTabbedPane1.addTab("Comida", vistaComida2);
        jTabbedPane1.addTab("Sustentacion Rutina-ejercicio", sustentacionRutinaEjercicio1);
        jTabbedPane1.addTab("Sustentacion Rut-Ejer", sustentacionRutinaEjercicoBuscar1);

        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane jTabbedPane1;
    private vistas.mostrarDatos.SustentacionRutinaEjercicio sustentacionRutinaEjercicio1;
    private vistas.mostrarDatos.SustentacionRutinaEjercicoBuscar sustentacionRutinaEjercicoBuscar1;
    private vistas.mostrarDatos.VistaComida vistaComida2;
    private vistas.mostrarDatos.VistaDieta vistaDieta2;
    private vistas.mostrarDatos.VistaEjercicio vistaEjercicio2;
    private vistas.mostrarDatos.VistaRutina vistaRutina1;
    private vistas.mostrarDatos.VistaUsuario vistaUsuario3;
    // End of variables declaration//GEN-END:variables
    private VentanaPrincipal vp;
}
