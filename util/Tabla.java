/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */
package util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class Tabla extends javax.swing.JPanel {

    /**
     * Creates new form NewJPanel
     */
    public Tabla() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setPreferredSize(new java.awt.Dimension(452, 260));
        setLayout(new java.awt.BorderLayout());

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
    
    public void llenarTabla(ResultSet rs) {
        DefaultTableModel dtm= new DefaultTableModel()
            {
                //Se hace las celdas ineditables
                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return false;
                }
            };  
        try{
            ResultSetMetaData md=rs.getMetaData();
            int columnas= md.getColumnCount();
            this.jTable1.setModel(dtm);
            for (int i = 1; i <= columnas; i++) {
                dtm.addColumn(md.getColumnLabel(i));
            }
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for (int j = 0; j < columnas; j++) {
                    fila[j]=rs.getObject(j+1);
                }
                dtm.addRow(fila);
            }
        } catch (SQLException ex) {
            
        }
    }
}
