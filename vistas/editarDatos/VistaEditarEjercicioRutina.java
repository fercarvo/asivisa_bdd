/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */
package vistas.editarDatos;

import mysql.manejador.Conector;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class VistaEditarEjercicioRutina extends javax.swing.JPanel {

    public VistaEditarEjercicioRutina() {
        initComponents();
        con= new Conector();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tblRutina = new util.Tabla();
        tblEjercicio = new util.Tabla();
        jLabel1 = new javax.swing.JLabel();
        txtRutina = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtEjercicio = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        btnCrear = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });

        jLabel1.setText("Nombre rutina");

        txtRutina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRutinaActionPerformed(evt);
            }
        });
        txtRutina.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRutinaKeyTyped(evt);
            }
        });

        jLabel2.setText("Nombre Ejercicio");

        txtEjercicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEjercicioActionPerformed(evt);
            }
        });
        txtEjercicio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEjercicioKeyTyped(evt);
            }
        });

        btnCrear.setText("Crear");
        btnCrear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearActionPerformed(evt);
            }
        });
        jPanel2.add(btnCrear);

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        jPanel2.add(btnEliminar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tblRutina, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(tblEjercicio, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtRutina, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(txtEjercicio, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(98, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tblRutina, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtRutina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(tblEjercicio, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtEjercicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        tblEjercicio.llenarTabla(con.mostrarEjercicio());
        tblRutina.llenarTabla(con.mostrarRutina());
    }//GEN-LAST:event_formComponentShown

    private void txtRutinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRutinaActionPerformed
        String nombre= txtRutina.getText();
        tblRutina.llenarTabla(con.buscarRutina_nombre(nombre));
        con.desconectar();
    }//GEN-LAST:event_txtRutinaActionPerformed

    private void txtRutinaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRutinaKeyTyped
        String nombre= txtRutina.getText();
        tblRutina.llenarTabla(con.buscarRutina_nombre(nombre));
        con.desconectar();
    }//GEN-LAST:event_txtRutinaKeyTyped

    private void txtEjercicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEjercicioActionPerformed
        String nombre= txtEjercicio.getText();
        tblEjercicio.llenarTabla(con.buscarEjercicio_nombre(nombre));
        con.desconectar();
    }//GEN-LAST:event_txtEjercicioActionPerformed

    private void txtEjercicioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEjercicioKeyTyped
        String nombre= txtEjercicio.getText();
        tblEjercicio.llenarTabla(con.buscarEjercicio_nombre(nombre));
        con.desconectar();
    }//GEN-LAST:event_txtEjercicioKeyTyped

    private void btnCrearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearActionPerformed
        String ejercicio= txtEjercicio.getText();
        String rutina= txtRutina.getText();
        con.crearEjercicioRutina(ejercicio, rutina);
        con.desconectar();
    }//GEN-LAST:event_btnCrearActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        String ejercicio= txtEjercicio.getText();
        String rutina= txtRutina.getText();
        con.eliminarEjercicioRutina(ejercicio, rutina);
        con.desconectar();
    }//GEN-LAST:event_btnEliminarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCrear;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    private util.Tabla tblEjercicio;
    private util.Tabla tblRutina;
    private javax.swing.JTextField txtEjercicio;
    private javax.swing.JTextField txtRutina;
    // End of variables declaration//GEN-END:variables
    private Conector con;
}
