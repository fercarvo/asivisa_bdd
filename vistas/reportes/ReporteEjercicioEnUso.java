/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */
package vistas.reportes;

import mysql.manejador.Conector;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class ReporteEjercicioEnUso extends javax.swing.JPanel {

    public ReporteEjercicioEnUso() {
        initComponents();
        con= new Conector();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabla = new util.Tabla();
        btnActualizar = new javax.swing.JButton();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());
        add(tabla, java.awt.BorderLayout.CENTER);

        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });
        add(btnActualizar, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        tabla.llenarTabla(con.reporteEjercicio_EnUso());
        con.desconectar();
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        tabla.llenarTabla(con.reporteEjercicio_EnUso());
        con.desconectar();
    }//GEN-LAST:event_formComponentShown


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private util.Tabla tabla;
    // End of variables declaration//GEN-END:variables
    private Conector con;
}
