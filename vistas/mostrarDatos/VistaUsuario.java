/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */
package vistas.mostrarDatos;

import mysql.manejador.Conector;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class VistaUsuario extends javax.swing.JPanel {

    public VistaUsuario() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabla1 = new util.Tabla();
        jButton2 = new javax.swing.JButton();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());
        add(tabla1, java.awt.BorderLayout.CENTER);

        jButton2.setText("Actualizar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        tabla1.llenarTabla(con.mostrarUsuario());
        con.desconectar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        tabla1.llenarTabla(con.mostrarUsuario());
        con.desconectar();
    }//GEN-LAST:event_formComponentShown


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private util.Tabla tabla1;
    // End of variables declaration//GEN-END:variables
    private Conector con= new Conector();
}
