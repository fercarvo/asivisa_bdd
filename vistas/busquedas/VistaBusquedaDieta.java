/*
 * ESCUELA SUPERIOR POLITECNICA DEL LITORAL
 * Proyecto de base de datos
 * II termino 2014-2015
 */
package vistas.busquedas;

import javax.swing.JOptionPane;
import mysql.manejador.Conector;

/**
 *
 * @author Edgar Fernando Carvajal Ulloa
 */
public class VistaBusquedaDieta extends javax.swing.JPanel {

    /**
     * Creates new form VistaBusquedaDieta
     */
    public VistaBusquedaDieta() {
        initComponents();
        con= new Conector();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabla = new util.Tabla();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtIngestaCalorica = new javax.swing.JTextField();
        txtHoraDia = new javax.swing.JTextField();
        btnMostrarTodos = new javax.swing.JButton();

        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });

        jLabel1.setText("Buscar por:");

        jLabel2.setText("Nombre");

        jLabel3.setText("Ingesta Calorica");

        jLabel4.setText("Hora del dia");

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        txtIngestaCalorica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIngestaCaloricaActionPerformed(evt);
            }
        });

        txtHoraDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHoraDiaActionPerformed(evt);
            }
        });
        txtHoraDia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtHoraDiaKeyTyped(evt);
            }
        });

        btnMostrarTodos.setText("Mostrar todos");
        btnMostrarTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarTodosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtIngestaCalorica, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtHoraDia))
                .addGap(35, 35, 35)
                .addComponent(btnMostrarTodos)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtIngestaCalorica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnMostrarTodos))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtHoraDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabla, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        String nombre= txtNombre.getText();
        tabla.llenarTabla(con.buscarDieta_nombre(nombre));
        con.desconectar();
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        String nombre= txtNombre.getText();
        tabla.llenarTabla(con.buscarDieta_nombre(nombre));
        con.desconectar();
    }//GEN-LAST:event_txtNombreKeyTyped

    private void txtIngestaCaloricaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIngestaCaloricaActionPerformed
        try {
            Double.parseDouble(txtIngestaCalorica.getText());
            float calorias= Float.parseFloat(txtIngestaCalorica.getText());
            tabla.llenarTabla(con.buscarDieta_ingestaCalorica(calorias));
            con.desconectar();
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "INGRESE DATOS CORRECTOS!!!!!", "Error Datos", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txtIngestaCaloricaActionPerformed

    private void txtHoraDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHoraDiaActionPerformed
        String horaDia= txtHoraDia.getText();
        tabla.llenarTabla(con.buscarDieta_horaDia(horaDia));
        con.desconectar();
    }//GEN-LAST:event_txtHoraDiaActionPerformed

    private void txtHoraDiaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHoraDiaKeyTyped
        String horaDia= txtHoraDia.getText();
        tabla.llenarTabla(con.buscarDieta_horaDia(horaDia));
        con.desconectar();
    }//GEN-LAST:event_txtHoraDiaKeyTyped

    private void btnMostrarTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarTodosActionPerformed
        tabla.llenarTabla(con.mostrarDieta());
        con.desconectar();
    }//GEN-LAST:event_btnMostrarTodosActionPerformed

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        tabla.llenarTabla(con.mostrarDieta());
        con.desconectar();
    }//GEN-LAST:event_formComponentShown


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnMostrarTodos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private util.Tabla tabla;
    private javax.swing.JTextField txtHoraDia;
    private javax.swing.JTextField txtIngestaCalorica;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
    private Conector con;
}
